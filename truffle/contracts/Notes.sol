// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;


contract Notes {
  mapping (address => string) private ipnsKeys;

  function getKey() public view returns (string memory) {
    return ipnsKeys[msg.sender];
  }

  function setKey(string calldata ipnsKey) public notExistingUser {
    ipnsKeys[msg.sender] = ipnsKey;
  }

  modifier notExistingUser {
    require(bytes(ipnsKeys[msg.sender]).length == 0, "Error: the user has already a Note assigned");
    _;
  } 
}
