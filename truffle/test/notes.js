const Notes = artifacts.require("Notes");

contract('Notes', (accounts) => {
  it('should assign an w3name to sender address', async () => {
    const notesInstance = await Notes.deployed();
    const key = 'w3name_key';
    await notesInstance.setKey(key, { from: accounts[0] });

    const newKey = await notesInstance.getKey.call({ from: accounts[0] });

    assert.equal(newKey, key, "The key is not the expected");

  });

  it('should throw error if Key is re-assigned to address', async () => {
    const notesInstance = await Notes.deployed();

    await notesInstance.setKey("key", { from: accounts[1] });

    try {
      await notesInstance.setKey("anotherKey", { from: accounts[1] })
      assert.fail("The transaction should have thrown an error");
    }
    catch (err) {
      assert.include(err.message, "revert", "The error message should contain 'revert'");
    }

  });
});
