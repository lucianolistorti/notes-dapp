import React, { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';

export const LoadingButton = ({ clickFn, className, children, loadingLabel, variant }) => {
    const [isLoading, setLoading] = useState(false);

    useEffect(() => {
        if (isLoading) {
            clickFn().then(() => {
                setLoading(false);
            });
        }
    }, [isLoading]);

    const handleClick = () => setLoading(true);

    return (
        <Button
            className={className}
            variant={variant || "primary"}
            disabled={isLoading}
            onClick={!isLoading ? handleClick : null}
        >
            {isLoading ? loadingLabel : children}
        </Button>
    );
}