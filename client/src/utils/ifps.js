import { create as createIPFS } from 'ipfs-core';
import last from 'it-last';
import { resolveLastCIDFromW3Name } from './w3name';

export const initializeIPFS = async () => {
    const ipfs = new IPFS();
    await ipfs.initialize();
    return ipfs;
}

class IPFS {

    initialize = async () => {
        this.ipfs = await createIPFS();
    }

    addEncryptedObjectToIPFS = async (object, did) => {
        const jwe = await did.createDagJWE(object, [did.id]);
        return await this.ipfs.add(JSON.stringify(jwe));
    }

    getNotesFromIPNS = async (ipnsCID, did) => {

        const notesCID = await this.resolveLastCIDFromIPNS(ipnsCID);

        const decoder = new TextDecoder();
        let stringJWE = '';
        for await (const buf of this.ipfs.cat(notesCID)) {
            stringJWE += decoder.decode(buf, {
                stream: true
            });
        }

        const jwe = await did.decryptDagJWE(JSON.parse(stringJWE));

        return [notesCID, jwe];
    }

    getNotesFromW3Name = async (w3name, did) => {

        const notesCID = await resolveLastCIDFromW3Name(w3name);

        const decoder = new TextDecoder();
        let stringJWE = '';
        for await (const buf of this.ipfs.cat(notesCID)) {
            stringJWE += decoder.decode(buf, {
                stream: true
            });
        }

        const jwe = await did.decryptDagJWE(JSON.parse(stringJWE));

        return [notesCID, jwe];
    }

    generateEncryptedKey = async (did, account) => {

        await this.keyIsPresent(account) ||
            await this.ipfs.key.gen(account, {
                type: 'rsa',
                size: 2048
            });

        const pass = this.generateRandomPass();
        const pem = await this.ipfs.key.export(account, pass);

        const encryptedPem = await did.createDagJWE({
            pass,
            pem
        }, [did.id]);

        return encryptedPem;
    }

    publishNoteCIDToIPNS = async (cid, key, account) => {
        // import address key
        await this.keyIsPresent(account) || await this.ipfs.key.import(account, key.pem, key.pass);

        // publish name
        return await this.ipfs.name.publish(cid, { key: account });
    }

    resolveLastCIDFromIPNS = async (name) => {
        const lastCID = await last(this.ipfs.name.resolve(name));
        return lastCID.replace("/ipfs/", "");
    }

    generateRandomPass = () => {
        return window.crypto.getRandomValues(new BigUint64Array(1))[0].toString(36).toUpperCase() +
            window.crypto.getRandomValues(new BigUint64Array(1))[0].toString(36);
    }

    keyIsPresent = async (account) => {
        return (await this.ipfs.key.list()).find(key => key.name === account);
    }

}

