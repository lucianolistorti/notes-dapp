import { ThreeIdConnect, EthereumAuthProvider } from '@3id/connect'
import KeyDidResolver from '@ceramicnetwork/key-did-resolver';
import { DID } from 'dids';

const connect = async () => {

    const threeIdConnect = new ThreeIdConnect();
    // Usually the following three steps are taken when
    // a user choose to connect their wallet
    const addresses = await window.ethereum.enable();
    // Create 3id connect instance
    const authProvider = new EthereumAuthProvider(window.ethereum, addresses[0]);
    await threeIdConnect.connect(authProvider);

    const provider = await threeIdConnect.getDidProvider();
    return new DID({ resolver: KeyDidResolver.getResolver(), provider });
}

const did = connect();

export default did;