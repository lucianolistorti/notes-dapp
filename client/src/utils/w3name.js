import * as Name from 'w3name';

export const publishInitialW3NameRevision = async (cid) => {
    const name = await Name.create();

    // since we don't have a previous revision, we use Name.v0 to create the initial revision
    const revision = await Name.v0(name, cid);

    await Name.publish(revision, name.key);

    return name;
}

export const publishW3NameRevision = async (name, cid) => {
    const revision = await Name.resolve(name);
    const nextRevision = await Name.increment(revision, cid);

    await Name.publish(nextRevision, name.key);
}

export const loadSigningKey = async (bytes) => {
    const name = await Name.from(bytes);
    return name;
}

export const resolveLastCIDFromW3Name = async (w3name) => {
    const revision = await Name.resolve(w3name);

    return revision.value;
}

export const getW3NameFromBytes = async (bytes) => {

    const name = await Name.from(bytes);
    return name;
}

export const w3NameBytesToString = (bytes) => {
    const arr = Array.from // if available
        ? Array.from(bytes) // use Array#from
        : [].map.call(bytes, (v => v)); // otherwise map()
    // now stringify
    const str = JSON.stringify(arr);
    return str;
};

export const stringToW3NameBytes = (str) => {
    const retrievedArr = JSON.parse(str);
    const retrievedTypedArray = new Uint8Array(retrievedArr);
    return retrievedTypedArray
};