import notesContract from '../contracts/notes'

export const fetchKeyFromNotesContract = async (account) => {
    try {
        return await notesContract.methods.getKey().call({ from: account });
    } catch (error) {
        console.error(`Error fetching IPNS from account ${account}: `, error);
    }
}

export const assignKeyToNotesContract = async (encryptedKey, account) => {
    try {
        await notesContract.methods.setKey(encryptedKey).send({
            from: account
        });
    } catch (error) {
        console.error(`Error saving Key in Contract: `, error);
    }
}