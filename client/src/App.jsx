
import "./App.css";
import web3 from './utils/web3';
import { useState } from "react";
import Home from './components/Home';

import { ThreeIdConnect, EthereumAuthProvider } from '@3id/connect'
import { getResolver } from '@ceramicnetwork/3id-did-resolver';
import KeyDidResolver from 'key-did-resolver';
import { DID } from 'dids';
import { CeramicClient } from '@ceramicnetwork/http-client';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Spinner from 'react-bootstrap/Spinner';
import { LoadingButton } from "./shared/LoadingButton";
import { useEffect } from "react";

const App = () => {

  const [accounts, setAccounts] = useState([]);
  const [profileConnected, setProfileConnected] = useState(false);
  const [ceramic, setCeramic] = useState(null);
  const [loading, setLoading] = useState(true);
  const CEARMIC_TEST_NETWORK = "https://ceramic-clay.3boxlabs.com";

  useEffect(() => {
    if (localStorage.getItem("profileConnected") === 'true') {
      readProfile();
    } else {
      setLoading(false);
    }
  }, []);

  const getAccounts = async () => {
    try {
      let accounts = await web3.eth.requestAccounts();
      setAccounts(accounts);
    } catch (error) {
      console.error("Error reading accounts: ", error);
    }
  }

  const connect3ID = async () => {
    const addresses = await window.ethereum.enable();
    const ceramicInstance = new CeramicClient(CEARMIC_TEST_NETWORK);
    const threeIdConnect = new ThreeIdConnect();

    // Create 3id connect instance
    const authProvider = new EthereumAuthProvider(window.ethereum, addresses[0]);
    await threeIdConnect.connect(authProvider);

    const provider = await threeIdConnect.getDidProvider();

    const did = new DID({
      resolver: {
        ...KeyDidResolver.getResolver(),
        ...getResolver(ceramicInstance),
      },
      provider
    });

    ceramicInstance.setDID(did);

    try {
      await ceramicInstance.did.authenticate();
      console.log(ceramicInstance.did.id);
      setCeramic(ceramicInstance);
      setProfileConnected(true);
      localStorage.setItem("profileConnected", true);
    } catch (error) {
      console.error("Error connecting to 3ID: ", error);
      console.log(did.id);
    }
  }

  const readProfile = async () => {
    setLoading(true);
    await getAccounts();
    await connect3ID();
    setLoading(false);
  }

  return (
    <>

      {loading ?
        <Row className="justify-content-center pt-5">
          <Spinner animation="grow" variant="light" />
        </Row>
        :
        profileConnected ?
          <Home
            account={accounts[0]}
            did={ceramic ? ceramic.did : null} /> :

          <Container fluid className="pt-5">
            <Row className="text-center pb-4">
              <Col>
                <h1>Welcome to the decentralized Notes Taking dApp</h1>
              </Col>
            </Row>
            <Row className="text-center">
              <Col>
                <LoadingButton
                  className="px-4"
                  variant="primary"
                  size="lg"
                  clickFn={readProfile}
                  loadingLabel="Connecting..."> Connect Wallet </LoadingButton>
              </Col>
            </Row>
          </Container>
      }
    </>
  );
}

export default App;