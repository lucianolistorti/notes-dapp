const PIN_SERVICE_URL = `${process.env.REACT_APP_PIN_SERVER_URL}/api/pin`;

export const pinCidToService = async (currentCID, newCID) => {
        try {
            return await fetch(`${PIN_SERVICE_URL}/${currentCID || ''}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body:
                    JSON.stringify({ cid: newCID })
            });
        } catch (error) {
            console.error(error);
            throw error;
        }
}