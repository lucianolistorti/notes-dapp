import { useState } from "react";
import { useEffect } from "react";
import { pinCidToService } from "../services/PinningService";
import { assignKeyToNotesContract, fetchKeyFromNotesContract } from "../utils/contracts";
import { initializeIPFS } from "../utils/ifps";
import Notes from "./Notes";
import { LoadingButton } from "../shared/LoadingButton";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Spinner from 'react-bootstrap/Spinner';
import { getW3NameFromBytes, publishInitialW3NameRevision, stringToW3NameBytes, w3NameBytesToString } from "../utils/w3name";
import Navbar from 'react-bootstrap/Navbar';

const Home = ({ account, did }) => {
    const [w3name, setW3Name] = useState(null);
    const [ipfs, setIPFS] = useState(null);
    const [initialized, setInitialized] = useState(false);

    useEffect(() => {
        const initializeComponent = async () => {
            if (ipfs && w3name != null) {
                setInitialized(true);
                return;
            }

            let ipfsInstance;
            if (!ipfs) {
                try {
                    ipfsInstance = await initializeIPFS();
                    setIPFS(ipfsInstance);
                } catch (error) {
                    return; // useEffect runs twice in dev environment, so there is an ipfs lock error that should be ignored.
                }
            }

            const encryptedKey = await fetchKeyFromNotesContract(account);
            let userW3Name = '';

            if (encryptedKey) {
                const strBytes = await did.decryptDagJWE(JSON.parse(encryptedKey));
                userW3Name = await getW3NameFromBytes(stringToW3NameBytes(strBytes));
            }

            setW3Name(userW3Name);
        }

        initializeComponent();
    }, [ipfs, w3name]);

    const createAccount = async () => {

        try {

            const firstNote = {
                creation_time: Date.now(),
                title: "First note",
                text: "This is your first note!",
                user: account,
                did_id: did.id
            }

            const firstNoteCID = await ipfs.addEncryptedObjectToIPFS([firstNote], did);
            const firstW3Name = await publishInitialW3NameRevision(firstNoteCID.path);

            const encryptedKey = await did.createDagJWE(w3NameBytesToString(firstW3Name.key.bytes), [did.id]);

            await assignKeyToNotesContract(JSON.stringify(encryptedKey), account);
            await pinCidToService('', firstNoteCID.path);
            setW3Name(firstW3Name);
        } catch (error) {
            console.error("Error assigning w3name to account: ", error);
        }
    }

    const renderHome = () => {
        if (w3name) {
            return <Notes
                account={account}
                did={did}
                w3name={w3name}
                ipfs={ipfs}
            />
        } else {
            return <Container fluid className="pt-5">
                <Row className="text-center pb-4">
                    <Col>
                        <h3>There is no account created for this Identity.</h3>
                    </Col>
                </Row>
                <Row className="text-center">
                    <Col>
                        <LoadingButton
                            className="px-4"
                            variant="primary"
                            size="lg"
                            clickFn={createAccount}
                            loadingLabel="Connecting..."> Create account </LoadingButton>
                    </Col>
                </Row>
            </Container>
        }

    }

    const disconnect = () => {
        localStorage.setItem("profileConnected", false);
        window.location.reload(false);
    }

    return (
        <>
            <Navbar>
                <Container>
                    <Navbar.Brand onClick={() => window.location.reload(false)}>Notes dApp</Navbar.Brand>
                    <LoadingButton variant="outline-danger" clickFn={disconnect}>Disconnect</LoadingButton>
                </Container>
            </Navbar>
            {initialized ? renderHome() :
                <Row className="justify-content-center pt-5">
                    <Spinner animation="grow" variant="light" />
                </Row>
            }
        </>
    );

}

export default Home;