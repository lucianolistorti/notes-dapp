import { useState, useEffect } from "react";
import { pinCidToService } from "../services/PinningService";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import { LeftTabs } from "./LeftTabs";
import { NoteModal } from "./NoteModal";
import { publishW3NameRevision } from "../utils/w3name";
import { RemoveModal } from "./RemoveModal";

const Notes = ({ w3name, ipfs, did, account }) => {

    const [userNotes, setUserNotes] = useState({ cid: "", notes: [] });
    const [initialized, setInitialized] = useState(false);
    const [showNoteModal, setShowNoteModal] = useState(false);
    const [showRemoveModal, setShowRemoveModal] = useState(false);
    const [edition, setEdition] = useState({ title: "", text: "", index: "" });
    const [isEdit, setIsEdit] = useState(false);
    const [removal, setRemoval] = useState({ title: "", index: "" });

    useEffect(() => {
        const initializeComponent = async () => {
            const [cid, notes] = w3name ? await ipfs.getNotesFromW3Name(w3name, did) : [];

            setUserNotes({ cid, notes });
            setInitialized(true);
        }
        initializeComponent();
    }, []);

    const persistUserNotes = async (newNotesList) => {
        const newCID = await ipfs.addEncryptedObjectToIPFS(newNotesList, did);

        await publishW3NameRevision(w3name, newCID.path);
        await pinCidToService(userNotes.cid, newCID.path);

        return newCID;
    }

    const handleSaveNote = async (note) => {
        if (!note.title && !note.text) return;

        try {
            const newNote = {
                creation_time: Date.now(),
                title: note.title || "(no title)",
                text: note.text,
                user: account,
                did_id: did.id
            }

            const newCID = await persistUserNotes([...userNotes.notes, newNote]);

            setUserNotes({ cid: newCID.path, notes: [...userNotes.notes, newNote] });
            setShowNoteModal(false);
        } catch (error) {
            console.error("Error creating a new note: ", error);
        }
    }


    const showEditNoteModal = (title, text, index) => {
        setEdition({ title, text, index });
        setIsEdit(true);
        setShowNoteModal(true);
    }

    const showNewNote = () => {
        setIsEdit(false);
        setEdition({ title: "", text: "", index: -1 });
        setShowNoteModal(true);
    }

    const showRemoveNote = (title, index) => {
        setRemoval({ title, index });
        setShowRemoveModal(true);

    }

    const handleRemoveNote = async () => {
        userNotes.notes.splice(removal.index, 1);

        const newCID = await persistUserNotes(userNotes.notes);

        setUserNotes({ cid: newCID.path, notes: userNotes.notes });
        setShowRemoveModal(false);
    }

    const handleEditNote = async (newNote) => {
        userNotes.notes[edition.index] = newNote;

        const newCID = await persistUserNotes(userNotes.notes);

        setUserNotes({ cid: newCID.path, notes: userNotes.notes });
        setShowNoteModal(false);
    }

    const renderNotes = () => {
        return (
            <Container>
                <Row className="pt-5">
                    <Col className="ps-3">
                        <Button variant="outline-primary" onClick={showNewNote}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-plus-circle" viewBox="0 0 16 16">
                                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                            </svg>

                            <span className="ps-2">New</span>
                        </Button>
                    </Col>
                </Row>

                <Row className="pt-4">
                    <Col>
                        <LeftTabs
                            elements={userNotes.notes}
                            handleRemove={showRemoveNote}
                            handleEdit={showEditNoteModal} />
                    </Col>
                </Row>
            </Container>
        );
    }

    return (
        <>
            {initialized && renderNotes()
            }
            <NoteModal
                show={showNoteModal}
                handleSave={isEdit ? handleEditNote : handleSaveNote}
                handleClose={() => setShowNoteModal(false)}
                title={edition.title}
                text={edition.text} />
            <RemoveModal
                show={showRemoveModal}
                handleClose={() => setShowRemoveModal(false)}
                handleRemove={handleRemoveNote}
                title={removal.title} />
        </>
    )
}

export default Notes;