import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { LoadingButton } from '../shared/LoadingButton';

export const RemoveModal = ({show, title, handleRemove, handleClose}) => {

    return (
        <>
            <Modal show={show} onHide={handleClose} centered>
                <Modal.Header closeButton style={{backgroundColor: "#dc3545a1"}}>
                    <Modal.Title>Remove</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5>Do you want to remove the note <b>{title}</b> ?</h5>

                </Modal.Body>

                <Modal.Footer>
                    <Button
                        variant="secondary"
                        onClick={handleClose} >
                        Close
                    </Button>
                    <LoadingButton
                        variant="danger"
                        clickFn={handleRemove}
                        loadingLabel="Save">
                        Confirm
                    </LoadingButton>
                </Modal.Footer>
            </Modal>
        </>
    );
}