import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import { LoadingButton } from '../shared/LoadingButton';
import { useEffect } from 'react';

export const NoteModal = (props) => {

    const [title, setTitle] = useState("");
    const [text, setText] = useState("");

    useEffect(() => {
        if (props.show) {
            setTitle(props.title);
            setText(props.text);
        } else {
            setTitle("");
            setText("");
        }

    }, [props.show]);

    const handleClose = () => {
        props.handleClose();
    }

    const handleSave = async () => {
        await props.handleSave({ title, text });
    }

    return (
        <>
            <Modal size="lg" show={props.show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Note</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <InputGroup className="mb-3">
                        <Form.Control
                            value={title}
                            onChange={e => setTitle(e.target.value)}
                            placeholder="Insert title..."
                            aria-label="Title"
                            aria-describedby="title"
                            autoFocus />
                    </InputGroup>

                    <InputGroup>
                        <Form.Control
                            rows="10"
                            style={{ height: "100%" }}
                            as="textarea"
                            aria-label="With textarea"
                            value={text}
                            onChange={e => setText(e.target.value)} />
                    </InputGroup>

                </Modal.Body>

                <Modal.Footer>
                    <Button
                        variant="secondary"
                        onClick={handleClose} >
                        Close
                    </Button>
                    <LoadingButton
                        variant="primary"
                        clickFn={handleSave}
                        loadingLabel="Save">
                        Save
                    </LoadingButton>
                </Modal.Footer>
            </Modal>
        </>
    );
}