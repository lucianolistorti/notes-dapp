import web3 from '../utils/web3';
import notesContract from './Notes.json'

const address = notesContract.networks[process.env.REACT_APP_NETWORK_ID].address;
const abi = notesContract.abi;

export default new web3.eth.Contract(abi, address);