# Notes dApp

Notes taking decentralized application built with Solidity, 3IDConnect, IPFS and w3names. Frontend was build with React and web3js. The data is private as it is encrypted using the DID (Decentralized Identity) and it's not store in any centralized database.

__Currently available working with Goerli testnet: https://notes-dapp-client.vercel.app/__


## How does it work?

- __Encryption__

  All the data that is stored in IPFS is completely private as it's encrypted using assymetric cryptography. This is possible using 3ID Connect, which allows users to use their Ethereum wallet to get access to a DID Provider and encrypt the data before persisting in IPFS.

  With this, the Ethereum wallet is the only owner of the data and can only be decrypted using it's private key.


- __IPFS and w3names__

  The data is stored on IPFS, but to keep it modifiable, we use w3names pointing to the last version of the data. When creating an account with an Ethereum address and the DID (Decentralized Identity) associated to it, a w3name is assigned to the user and stored in the Smart Contract using a address->w3name mapping.

  This is one of the great advantages of this app: there is only one payable transaction made to the Smart Contract to save the address->w3name asscotiation. The rest of the modifications are done by creating a new CID, pinning in Filebase and publishing it to the w3name.


## Projects

__Client__

  Frontend application built with React that interacts with the Ethereum wallet, the Smart Contract, IPFS and the Ceramic ecosystem.

__Server__

  Basic Express Rest API that acts as a proxy to the Filebase pinning service.

__Truffle__

  All the Ethereum Smart Contract files built with Solidity.
